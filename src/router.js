import Vue from "vue";
import Router from "vue-router";
import Homepage from "./views/Homepage.vue";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
import SignUp from "./views/SignUp.vue";
import ProjectList from "./views/ProjectList";
import ProjectDetails from "./views/ProjectDetails";
import AddProject from "./views/AddProject";
import ViewProject from "./views/ViewProject";
import StudentDashboard from "./views/StudentDashboard";
import StudentDashboardHome from "./views/StudentDashboardHome";
import TeamMembers from "./views/TeamMembers";
import Clones from "./views/Clones";
import Collaborators from "./views/Collaborators";
import ProjectFeatures from "./views/ProjectFeatures";
import AdminDashboard from "./views/AdminDashboard";
import AdminDashboardHome from "./views/AdminDashboardHome";
import Projects from "./views/Projects";
import GuestUsers from "./views/GuestUsers";
import Students from "./views/Students";
import CollaborationCalls from "./views/CollaborationCalls";
import CollaborationRequests from "./views/CollaborationRequests";
import AllCollaborators from "./views/AllCollaborators";
import Reviews from "./views/Reviews";
import Admins from "./views/Admins";
import ProjectCategories from "./views/ProjectCategories";
import CategoryProducts from "./views/CategoryProducts.vue";
import GuestProfile from "./views/GuestProfile.vue"
import DeviceLogs from "./views/DeviceLogs.vue"
import SimulateProject from "./views/SimulateProject.vue"

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      component: Homepage,
      children: [
        {
          path: "/",
          name: "home",
          component: Home,
        },
        {
          path: "project-details/:id",
          name: "project-details",
          component: ProjectDetails,
        },
        {
          path: "project-list",
          name: "project-list",
          component: ProjectList,
        },
        {
          path: "add-project",
          name: "add-project",
          component: AddProject,
        },
        {
          path: "view-project/:id",
          name: "view-project",
          component: ViewProject,
        },
        {
          path: "guest-profile",
          name: "guest-profile",
          component: GuestProfile,
        },
        {
          path: "projet-category/:id",
          name: "project-category",
          component: CategoryProducts,
        },
        {
          path: "device-readings/:id",
          name: "device-readings",
          component: DeviceLogs,
        },
      ],
    },
    {
      path: "/login",
      name: "login",
      component: Login,
    },
    {
      path: "/signup",
      name: "signup",
      component: SignUp,
    },

    {
      path: "/student-dashboard",
      component: StudentDashboard,
      children: [
        {
          path: "/",
          name: "student-dashboard",
          component: StudentDashboardHome,
        },
        {
          path: "team-members",
          name: "team-members",
          component: TeamMembers,
        },
        {
          path: "clones",
          name: "clones",
          component: Clones,
        },
        {
          path: "collaborators",
          name: "collaborators",
          component: Collaborators,
        },
        {
          path: "collaboration-calls",
          name: "collaboration-calls",
          component: CollaborationCalls,
        },
        {
          path: "collaboration-requests",
          name: "collaboration-requets",
          component: CollaborationRequests,
        },
        {
          path: "features",
          name: "features",
          component: ProjectFeatures,
        },
        {
          path: "reviews",
          name: "reviews",
          component: Reviews,
        },
        {
          path: "simulate-project",
          name: "simulate-project",
          component: SimulateProject,
        },
      ],
    },
    {
      path: "/admin-dashboard",
      component: AdminDashboard,
      children: [
        {
          path: "/",
          name: "admin-dashboard",
          component: AdminDashboardHome,
        },
        {
          path: "projects",
          name: "projects",
          component: Projects,
        },
        {
          path: "project-categories",
          name: "project-categories",
          component: ProjectCategories,
        },
        {
          path: "guest-users",
          name: "guest-users",
          component: GuestUsers,
        },
        {
          path: "students",
          name: "students",
          component: Students,
        },
        {
          path: "administrators",
          name: "administrators",
          component: Admins,
        },
        {
          path: "all-collaborators",
          name: "all-collaborators",
          component: AllCollaborators,
        },
      ],
    },
  ],
});

export default router;
