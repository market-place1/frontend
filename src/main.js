import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import store from "./store/store";
import moment from "moment";
import VueClipboard from 'vue-clipboard2'

import JsonExcel from "vue-json-excel";
 
Vue.component("downloadExcel", JsonExcel);

Vue.use(VueClipboard)
// import Axios from 'axios'
import axiosInstance from "./store/axios_setup";
//Suppress all warnings
Vue.config.silent = true
// Add a request interceptor
axiosInstance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem("access_token");
    if (token) {
      config.headers["Authorization"] = "Bearer " + token;
    }
    return config;
  },
  (error) => {
    Promise.reject(error);
  }
);

axiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  function(error) {
    const originalRequest = error.config;

    if (
      error.response.status === 401 &&
      originalRequest.url === "http://173.212.252.138/api/refresh/token"
    ) {
      router.push("/");
      return Promise.reject(error);
    }

    if (
      error.response.status === 401 &&
      error.response.data.message == "Token has expired"
    ) {
      originalRequest._retry = true;
      const refresh_token = localStorage.getItem("refresh_token");
      return axiosInstance
        .post("/refresh/token", {
          refresh_token: refresh_token,
        })
        .then((res) => {
          if (res.status === 201 || res.status === 200) {
            localStorage.setItem("access_token", res.data.access_token);
            localStorage.setItem("refresh_token", res.data.refresh_token);
            axiosInstance.defaults.headers.common["Authorization"] =
              "Bearer " + localStorage.getItem("access_token");
            return axiosInstance(originalRequest);
          }
        });
    }

    if(error.response.status === 401){
      router.push("/");
      store.dispatch("logout")
      return Promise.reject(error);
    }
    return Promise.reject(error);
  }
);

Vue.filter("myDate", function(created) {
  return moment(created).format("MMMM Do YYYY");
});

import {
  required,
  email,
  max,
  min,
  size,
  oneOf,
} from "vee-validate/dist/rules";
import {
  extend,
  ValidationObserver,
  ValidationProvider,
  setInteractionMode,
} from "vee-validate";

setInteractionMode("eager");

extend("required", {
  ...required,
  message: "Enter {_field_}",
});

extend("email", {
  ...email,
  message: "Email must be valid",
});

extend("oneOf", {
  ...oneOf,
});

extend("max", {
  ...max,
  message: "{_field_} may not be greater than {length} characters",
});

extend("min", {
  ...min,
  message: "{_field_} may not be less than {length} characters",
});

extend("password", {
  params: ["target"],
  validate(value, { target }) {
    return value === target;
  },
  message: "Password does not match",
});

extend("size", {
  ...size,
  message: "File size should be less than 5 MB!",
});

Vue.component("ValidationProvider", ValidationProvider);
Vue.component("ValidationObserver", ValidationObserver);
Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount("#app");
