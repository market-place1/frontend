<template>
  <v-card
    class="content-bg card mx-auto"
    :max-width="card.maxWidth"
    outlined
    @click="loadDetails(project.id)"
  >
    <v-img
      :src="`http://173.212.252.138/${project.projectImage}`"
      height="210"
    ></v-img>
    <v-row no-gutters>
      <v-col>
        <v-card-title
          class="pl-2 pt-3 subtitle-1 font-weight-bold"
          style="line-height: 1.2rem"
          v-html="highlight(project.projectName)"
        >
          {{ project.projectName }}
        </v-card-title>

        <v-card-subtitle class="pl-2 pb-0" v-html="highlight(project.teamName)">
          {{ project.teamName }}
        </v-card-subtitle>
        <div class="text-center mt-2">
          <v-rating
            v-model="project.rating"
            color="blue darken-3"
            background-color="grey darken-1"
            empty-icon="$ratingFull"
            half-increments
            hover
            small
          ></v-rating>
        </div>
        <v-divider></v-divider>
        <v-card-actions class="justify-space-between pl-2 pr-2 pt-0 mt-2">
          <v-btn text class="text-capitalize">
            <v-icon size="18" class="pr-1">mdi-comment-outline</v-icon
            >{{ project.reviews > 0 ? project.reviews : 0 }}
            Reviews
          </v-btn>
          <v-btn text class="text-capitalize">
            <v-icon size="18" class="pr-1"
              >mdi-briefcase-download-outline</v-icon
            >{{ project.clones > 0 ? project.clones : 0 }} Clones
          </v-btn>
        </v-card-actions>
      </v-col>
    </v-row>
  </v-card>
</template>

<script>
import { mapState, mapGetters } from "vuex";
export default {
  name: "ProjectCard",
  props: {
    project: {
      type: Object,
      required: true,
    },
    card: Object,
  },
  data() {
    return {
      url: "process.env.VUE_APP_URL",
      rating: 4.5,
    };
  },
  methods: {
    loadDetails(id) {
      if (this.isAuthenticated) {
        this.$router.push(`/project-details/${id}`);
      } else {
        this.$router.push("/login");
      }
    },
    highlight(text) {
      return text.replace(
        new RegExp(this.searchTerm, "gi"),
        `<span class="blue--text">$&</span>`
      );
    },
  },
  computed: {
    ...mapGetters(["isAuthenticated"]),
    ...mapState(["searchTerm"]),
  },
};
</script>

<style></style>
