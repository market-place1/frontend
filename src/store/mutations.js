export const mutations = {
  TOGGLE_STEPPER(state, data) {
    state.toggleModalDialog = data;
  },
  TOGGLE_SNACKBAR(state, data) {
    state.snackbar = data;
  },
  SET_SNACKBAR_MESSAGE(state, data) {
    state.snackbar_message = data;
  },
  SELECT_PROJECT(state, data) {
    state.selected_project = data;
  },
  LOGIN_USER(state, payload) {
    console.log(payload);
    state.user = payload.user;
    state.token = payload.token;
    state.isLoggedIn = true;
  },
  LOGIN_LOADING(state, payload) {
    state.loginLoading = payload;
  },
  LOGIN_ERROR(state, payload) {
    state.loginError = payload;
    state.isLoggedIn = false;
  },
  SIGNUP_STATUS(state, payload) {
    state.isSignedUp = payload;
  },
  SIGNUP_LOADING(state, payload) {
    state.signupLoading = payload;
  },
  SIGNUP_ERROR(state, payload) {
    state.signUpError = payload;
  },
  LOGOUT(state) {
    state.isLoggedIn = false;
    state.token = "";
    state.user = {}
  },
  POST_PROJECT_STATUS(state, payload) {
    state.isProjectAdded = payload;
  },
  POST_PROJECT_LOADING(state, payload) {
    state.addProjectLoading = payload;
  },
  POST_PROJECT_ERROR(state, payload) {
    state.addProjectError = payload;
  },
  FETCH_PROJECTS(state, payload) {
    state.projects = payload;
  },
  FETCH_PROJECTS_LOADING(state, payload) {
    state.fetchProjectsLoading = payload;
  },
  FETCH_PROJECTS_ERROR(state, payload) {
    state.fetchProjectsError = payload;
  },
  FETCH_PROJECT_DETAILS(state, payload) {
    console.log(payload);
    state.project = payload;
  },
  FETCH_PROJECT_DETAILS_LOADING(state, payload) {
    state.fetchProjectDetailsLoading = payload;
  },
  FETCH_PROJECT_DETAILS_ERROR(state, payload) {
    state.fetchProjectDetailsError = payload;
  },
  FETCH_USER_PROJECTS(state, payload) {
    console.log("Payload", payload);
    state.user_projects = payload;
  },
  FETCH_USER_PROJECTS_LOADING(state, payload) {
    state.fetchUserProjectsLoading = payload;
  },
  FETCH_USER_PROJECTS_ERROR(state, payload) {
    state.fetchUserProjectsError = payload;
  },

  FETCH_TEAM_MEMBERS(state, payload) {
    state.team_members = payload;
  },
  FETCH_TEAM_MEMBERS_LOADING(state, payload) {
    state.fetchTeamMembersLoading = payload;
  },
  FETCH_TEAM_MEMBERS_ERROR(state, payload) {
    state.fetchTeamMembersError = payload;
  },
  FETCH_CLONES(state, payload) {
    state.clones = payload;
  },
  FETCH_CLONES_LOADING(state, payload) {
    state.fetchClonesLoading = payload;
  },
  FETCH_CLONES_ERROR(state, payload) {
    state.fetchClonesError = payload;
  },
  FETCH_FEATURES(state, payload) {
    state.features = payload;
  },
  FETCH_FEATURES_LOADING(state, payload) {
    state.fetchFeaturesLoading = payload;
  },
  FETCH_FEATURES_ERROR(state, payload) {
    state.fetchFeaturesError = payload;
  },
  POST_FEATURE_STATUS(state, payload) {
    state.isFeaturePosted = payload;
  },
  POST_FEATURE_LOADING(state, payload) {
    state.postFeatureLoading = payload;
  },
  POST_FEATURE_ERROR(state, payload) {
    state.postFeatureError = payload;
  },
  FETCH_COLLABORATION_CALLS(state, payload) {
    state.collaboration_calls = payload;
  },
  FETCH_COLLABORATION_CALLS_LOADING(state, payload) {
    state.fetchCollaborationCallsLoading = payload;
  },
  FETCH_COLLABORATION_CALLS_ERROR(state, payload) {
    state.fetchCollaborationCallsError = payload;
  },
  POST_COLLABORATION_CALL_STATUS(state, payload) {
    state.isCollaborationCallPosted = payload;
  },
  POST_COLLABORATION_CALL_LOADING(state, payload) {
    state.postCollaborationCallLoading = payload;
  },
  POST_COLLABORATION_CALL_ERROR(state, payload) {
    state.postCollaborationCallError = payload;
  },
  CHANGE_COLLABORATION_CALL_STATUS_STATUS(state, payload) {
    state.isCollaborationCallStatusChanged = payload;
  },
  CHANGE_COLLABORATION_CALL_STATUS_LOADING(state, payload) {
    state.changeCollaborationCallStatusLoading = payload;
  },
  CHANGE_COLLABORATION_CALL_STATUS_ERROR(state, payload) {
    state.changeCollaborationCallStatusError = payload;
  },
  FETCH_COLLABORATION_REQUESTS(state, payload) {
    state.collaboration_requests = payload;
  },
  FETCH_COLLABORATION_REQUESTS_LOADING(state, payload) {
    state.fetchCollaborationRequestsLoading = payload;
  },
  FETCH_COLLABORATION_REQUESTS_ERROR(state, payload) {
    state.fetchCollaborationRequestsError = payload;
  },
  CHANGE_COLLABORATION_REQUEST_STATUS_STATUS(state, payload) {
    state.isCollaborationRequestStatusChanged = payload;
  },
  CHANGE_COLLABORATION_REQUEST_STATUS_LOADING(state, payload) {
    state.changeCollaborationRequestStatusLoading = payload;
  },
  CHANGE_COLLABORATION_REQUEST_STATUS_ERROR(state, payload) {
    state.changeCollaborationRequestStatusError = payload;
  },
  FETCH_REVIEWS(state, payload) {
    state.reviews = payload;
  },
  FETCH_REVIEWS_LOADING(state, payload) {
    state.fetchReviewsLoading = payload;
  },
  FETCH_REVIEWS_ERROR(state, payload) {
    state.fetchReviewsError = payload;
  },
  FETCH_STUDENT_DASHBORD_STATISTICS(state, payload) {
    state.studentDashboardStatistics = payload;
  },
  FETCH_STUDENT_DASHBORD_STATISTICS_LOADING(state, payload) {
    state.fetchStudentDashboardStatisticsLoading = payload;
  },
  FETCH_STUDENT_DASHBORD_STATISTICS_ERROR(state, payload) {
    state.fetchStudentDashboardStatisticsError = payload;
  },
  FETCH_STUDENTS(state, payload) {
    state.students = payload;
  },
  FETCH_STUDENTS_LOADING(state, payload) {
    state.fetchStudentsLoading = payload;
  },
  FETCH_STUDENTS_ERROR(state, payload) {
    state.fetchStudentsError = payload;
  },
  FETCH_ADMINISTRATORS(state, payload) {
    state.admininstrators = payload;
  },
  FETCH_ADMINISTRATORS_LOADING(state, payload) {
    state.fetchAdministratorsLoading = payload;
  },
  FETCH_ADMINISTRATORS_ERROR(state, payload) {
    state.fetchAdministratorsError = payload;
  },
  POST_ADMINISTRATOR_STATUS(state, payload) {
    state.isAdministratorPosted = payload;
  },
  POST_ADMINISTRATOR_LOADING(state, payload) {
    state.postAdministratorLoading = payload;
  },
  POST_ADMINISTRATOR_ERROR(state, payload) {
    state.postAdministratorError = payload;
  },
  CHANGE_ADMINISTRATOR_STATUS_STATUS(state, payload) {
    state.isAdministratorStatusChanged = payload;
  },
  CHANGE_ADMINISTRATOR_STATUS_LOADING(state, payload) {
    state.changeAdministratorStatusLoading = payload;
  },
  CHANGE_ADMINISTRATOR_STATUS_ERROR(state, payload) {
    state.changeAdministratorStatusError = payload;
  },
  FETCH_ALL_COLLABORATORS(state, payload) {
    state.all_collaborators = payload;
  },
  FETCH_ALL_COLLABORATORS_LOADING(state, payload) {
    state.fetchAllCollaboratorsLoading = payload;
  },
  FETCH_ALL_COLLABORATORS_ERROR(state, payload) {
    state.fetchAllCollaboratorsError = payload;
  },
  FETCH_GUEST_USERS(state, payload) {
    state.guest_users = payload;
  },
  FETCH_GUEST_USERS_LOADING(state, payload) {
    state.fetchGuestUsersLoading = payload;
  },
  FETCH_GUEST_USERS_ERROR(state, payload) {
    state.fetchGuestUsersError = payload;
  },
  FETCH_CATEGORIES(state, payload) {
    state.categories = payload;
  },
  FETCH_CATEGORIES_LOADING(state, payload) {
    state.fetchCategoriesLoading = payload;
  },
  FETCH_CATEGORIES_ERROR(state, payload) {
    state.fetchCategoriesError = payload;
  },
  POST_CATEGORY_STATUS(state, payload) {
    state.isCategoryPosted = payload;
  },
  POST_CATEGORY_LOADING(state, payload) {
    state.postCategoryLoading = payload;
  },
  POST_CATEGORY_ERROR(state, payload) {
    state.postCategoryError = payload;
  },
  FETCH_ALL_PROJECTS(state, payload) {
    state.all_projects = payload;
  },
  FETCH_ALL_PROJECTS_LOADING(state, payload) {
    state.fetchAllProjectsLoading = payload;
  },
  FETCH_ALL_PROJECTS_ERROR(state, payload) {
    state.fetchAllProjectsError = payload;
  },
  CHANGE_PROJECT_STATUS_STATUS(state, payload) {
    state.isProjectStatusChanged = payload;
  },
  CHANGE_PROJECT_STATUS_LOADING(state, payload) {
    state.changeProjectStatusLoading = payload;
  },
  CHANGE_PROJECT_STATUS_ERROR(state, payload) {
    state.changeProjectStatusError = payload;
  },
  FETCH_ADMIN_DASHBORD_STATISTICS(state, payload) {
    state.adminDashboardStatistics = payload;
  },
  FETCH_ADMIN_DASHBORD_STATISTICS_LOADING(state, payload) {
    state.fetchAdminDashboardStatisticsLoading = payload;
  },
  FETCH_ADMIN_DASHBORD_STATISTICS_ERROR(state, payload) {
    state.fetchAdminDashboardStatisticsError = payload;
  },
  FETCH_PROJECT_DOCUMENTATION(state, payload) {
    state.projectDocumentation = payload;
  },
  FETCH_PROJECT_DOCUMENTATION_LOADING(state, payload) {
    state.fetchProjectDocumentationLoading = payload;
  },
  FETCH_PROJECT_DOCUMENTATION_ERROR(state, payload) {
    state.fetchProjectDocumentationError = payload;
  },
  POST_MEMBER_STATUS(state, payload) {
    state.isMemberAdded = payload;
  },
  POST_MEMBER_LOADING(state, payload) {
    state.postMemberLoading = payload;
  },
  POST_TEAM_MEMBER_ERROR(state, payload) {
    state.postMemberError = payload;
  },
  POST_COLLABORATION_REQUEST_STATUS(state, payload) {
    state.isCollaborationRequestPosted = payload;
  },
  POST_COLLABORATION_REQUEST_LOADING(state, payload) {
    state.postCollaborationRequestLoading = payload;
  },
  POST_TEAM_COLLABORATION_REQUEST_ERROR(state, payload) {
    state.postCollaborationRequestError = payload;
  },
  POST_REVIEW_STATUS(state, payload) {
    state.isReviewPosted = payload;
  },
  POST_REVIEW_LOADING(state, payload) {
    state.postReviewLoading = payload;
  },
  POST_TEAM_REVIEW_ERROR(state, payload) {
    state.postReviewError = payload;
  },
  POST_REPLY_STATUS(state, payload) {
    state.isReplyPosted = payload;
  },
  POST_REPLY_LOADING(state, payload) {
    state.postReplyLoading = payload;
  },
  POST_TEAM_REPLY_ERROR(state, payload) {
    state.postReplyError = payload;
  },
  FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS(state, payload) {
    state.adminDashboardMonthlyStatistics = payload;
  },
  FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_LOADING(state, payload) {
    state.fetchAdminDashboardMonthlyStatisticsLoading = payload;
  },
  FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_ERROR(state, payload) {
    state.fetchAdminDashboardMonthlyStatisticsError = payload;
  },
  FETCH_PROJECT_DISTRIBUTION_COUNT(state, payload) {
    state.projectDistributionCount = payload;
  },
  FETCH_PROJECT_DISTRIBUTION_COUNT_LOADING(state, payload) {
    state.fetchprojectDistributionCountLoading = payload;
  },
  FETCH_PROJECT_DISTRIBUTION_COUNT_ERROR(state, payload) {
    state.fetchprojectDistributionCountError = payload;
  },
  SEARCHTERM(state, term) {
    state.searchTerm = term;
  },
  FETCH_COLLABORATORS(state, payload) {
    state.collaborators = payload;
  },
  FETCH_COLLABORATORS_LOADING(state, payload) {
    state.fetchCollaboratorsLoading = payload;
  },
  FETCH_COLLABORATORS_ERROR(state, payload) {
    state.fetchCollaboratorsError = payload;
  },
  POST_RATING_STATUS(state, payload) {
    state.isRatingPosted = payload;
  },
  POST_RATING_LOADING(state, payload) {
    state.postRatingLoading = payload;
  },
  POST_RATING_ERROR(state, payload) {
    state.postRatingError = payload;
  },
  FETCH_ADMIN_MONTHLY_PROJECTS(state, payload) {
    state.adminMonthlyProjectStatistics = payload;
  },
  FETCH_ADMIN_MONTHLY_PROJECTS_LOADING(state, payload) {
    state.fetchAdminMonthlyProjectsLoading = payload;
  },
  FETCH_ADMIN_MONTHLY_PROJECTS_ERROR(state, payload) {
    state.fetchAdminMonthlyProjectsError = payload;
  },
  FETCH_STUDENT_MONTHLY_CLONES(state, payload) {
    state.studentMonthlyClonesStatistics = payload;
  },
  FETCH_STUDENT_MONTHLY_CLONES_LOADING(state, payload) {
    state.fetchStudentMonthlyClonesLoading = payload;
  },
  FETCH_STUDENT_MONTHLY_CLONES_ERROR(state, payload) {
    state.fetchStudentMonthlyClonesError = payload;
  },
  POST_FEEDBACK_STATUS(state, payload) {
    state.isFeedbackPosted = payload;
  },
  POST_FEEDBACK_LOADING(state, payload) {
    state.postFeedbackLoading = payload;
  },
  POST_FEEDBACK_ERROR(state, payload) {
    state.postFeedbackError = payload;
  },
  UPDATE_ADMIN_STATUS(state, payload) {
    state.isAdminUpdated = payload;
  },
  UPDATE_ADMIN_LOADING(state, payload) {
    state.updateAdminLoading = payload;
  },
  UPDATE_ADMIN_ERROR(state, payload) {
    state.updateAdminError = payload;
  },
  DELETE_ADMIN_STATUS(state, payload) {
    state.isAdminDeleted = payload;
  },
  DELETE_ADMIN_LOADING(state, payload) {
    state.deleteAdminLoading = payload;
  },
  DELETE_ADMIN_ERROR(state, payload) {
    state.deletedAdminError = payload;
  },
  UPDATE_CATEGORY_STATUS(state, payload) {
    state.isCategoryUpdated = payload;
  },
  UPDATE_CATEGORY_LOADING(state, payload) {
    state.updateCategoryLoading = payload;
  },
  UPDATE_CATEGORY_ERROR(state, payload) {
    state.updateCategoryError = payload;
  },
  UPDATE_CALL_STATUS(state, payload) {
    state.isCallUpdated = payload;
  },
  UPDATE_CALL_LOADING(state, payload) {
    state.updateCallLoading = payload;
  },
  UPDATE_CALL_ERROR(state, payload) {
    state.updateCallError = payload;
  },
  UPDATE_FEATURE_STATUS(state, payload) {
    state.isFeatureUpdated = payload;
  },
  UPDATE_FEATURE_LOADING(state, payload) {
    state.updateFeatureLoading = payload;
  },
  UPDATE_FEATURE_ERROR(state, payload) {
    state.updateFeatureError = payload;
  },
  UPDATE_TEAM_MEMBER_STATUS(state, payload) {
    state.isTeamMemberUpdated = payload;
  },
  UPDATE_TEAM_MEMBER_LOADING(state, payload) {
    state.updateTeamMemberLoading = payload;
  },
  UPDATE_TEAM_MEMBER_ERROR(state, payload) {
    state.updateTeamMemberError = payload;
  },
  FETCH_API_KEY(state, payload) {
    state.apiKey = payload;
  },
  FETCH_API_KEY_LOADING(state, payload) {
    state.fetchApiKeyLoading = payload;
  },
  FETCH_API_KEY_ERROR(state, payload) {
    state.fetchApiKeyError = payload;
  },
};
