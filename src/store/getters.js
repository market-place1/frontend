export const getters = {
  isAuthenticated: (state) => !!state.token,
  user_id: (state) => state.user.id,
  user_type: (state) => state.user.role,
  searchTerm: (state) => state.searchTerm,
  email: (state) => state.user.email,
  username: (state) => state.user.first_name + " " + state.user.last_name,
  getFeatureNames(state) {
    return state.features.map((feature) => {
      return feature.featureName;
    });
  },
  getCategoryNames(state) {
    return state.categories.map((category) => {
      return category.category;
    });
  },
  getCategoryByName: (state) => (name) => {
    return state.categories.find((item) => item.category === name);
  },
  getFeatureByName: (state) => (name) => {
    return state.features.find((item) => item.featureName === name);
  },
  chart_labels: (state) => {
    return Object.keys(state.projectDistributionCount);
  },
  chart_values: (state) => {
    return Object.values(state.projectDistributionCount);
  },
  getProjectValues(state) {
    return state.adminMonthlyProjectStatistics.map((project) => {
      return project.projects;
    });
  },
  getMonthLabels(state) {
    return state.adminMonthlyProjectStatistics.map((month) => {
      return month.month;
    });
  },
  getClonesValues(state) {
    return state.studentMonthlyClonesStatistics.map((clone) => {
      return clone.clones;
    });
  },
  getMonthCloneLabels(state) {
    return state.studentMonthlyClonesStatistics.map((month) => {
      return month.month;
    });
  },
  getProjectsByCategories: (state) => (id) => {
    return state.projects.filter((project) => project.categoryId == id);
  },
  getCategory: (state) => (id) => {
    return state.categories.find((item) => item.id === id);
  },
  getAdministratorById: (state) => (id) => {
    return state.admininstrators.find((item) => item.id === id);
  },
  getCategoryById: (state) => (id) => {
    return state.categories.find((item) => item.id === id);
  },
  getCallById: (state) => (id) => {
    return state.collaboration_calls.find((item) => item.id === id);
  },
  getFeatureById: (state) => (id) => {
    return state.features.find((item) => item.id === id);
  },
  getTeamMemberById: (state) => (id) => {
    return state.team_members.find((item) => item.id === id);
  },
};
