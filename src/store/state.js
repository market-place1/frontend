export const state = {
  toggleModalDialog: false,
  snackbar: false,
  snackbar_message: "",
  selected_project: "",
  isLoggedIn: false,
  token: localStorage.getItem("access_token") || "",
  loginLoading: false,
  user: {},
  loginError: "",
  isSignedUp: false,
  signupLoading: false,
  signUpError: {},
  isProjectAdded: false,
  addProjectLoading: false,
  addProjectError: "",
  projects: [],
  fetchProjectsLoading: false,
  fetchProjectsError: "",
  project: {},
  fetchProjectDetailsLoading: false,
  fetchProjectDetailsError: "",
  projectDocumentation: {},
  fetchProjectDocumentationLoading: false,
  fetchProjectDocumentationError: "",
  user_projects: [],
  fetchUserProjectsLoading: false,
  fetchUserProjectsError: "",
  isMemberAdded: false,
  postMemberLoading: false,
  postMemberError: "",
  team_members: [],
  fetchTeamMembersLoading: false,
  fetchTeamMembersError: "",
  clones: [],
  fetchClonesLoading: false,
  fetchClonesError: "",
  features: [],
  fetchFeaturesLoading: false,
  fetchFeaturesError: "",
  isFeaturePosted: false,
  postFeatureLoading: false,
  postFeatureError: "",
  collaboration_calls: [],
  fetchCollaborationCallsLoading: false,
  fetchCollaborationCallsError: "",
  isCollaborationCallPosted: false,
  postCollaborationCallLoading: false,
  postCollaborationCallError: "",
  isCollaborationCallStatusChanged: false,
  changeCollaborationCallStatusLoading: false,
  changeCollaborationCallStatusError: "",
  collaboration_requests: [],
  fetchCollaborationRequestsLoading: false,
  fetchCollaborationRequestsError: "",
  isCollaborationRequestStatusChanged: false,
  changeCollaborationRequestStatusLoading: false,
  changeCollaborationRequestStatusError: "",
  reviews: [],
  fetchReviewsLoading: false,
  fetchReviewsError: "",
  studentDashboardStatistics: {},
  fetchStudentDashboardStatisticsLoading: false,
  fetchStudentDashboardStatisticsError: "",
  students: [],
  fetchStudentsLoading: false,
  fetchStudentsError: "",
  admininstrators: [],
  fetchAdministratorsLoading: false,
  fetchAdministratorsError: "",
  isAdministratorPosted: false,
  postAdministratorLoading: false,
  postAdministratorError: "",
  isAdministratorStatusChanged: false,
  changeAdministratorStatusLoading: false,
  changeAdministratorStatusError: "",
  all_collaborators: [],
  fetchAllCollaboratorsLoading: false,
  fetchAllCollaboratorsError: "",
  guest_users: [],
  fetchGuestUsersLoading: false,
  fetchGuestUsersError: "",
  categories: [],
  fetchCategoriesLoading: false,
  fetchCategoriesError: "",
  isCategoryPosted: false,
  postCategoryLoading: false,
  postCategoryError: "",
  all_projects: [],
  fetchAllProjectsLoading: false,
  fetchAllProjectsError: "",
  isProjectStatusChanged: false,
  changeProjectStatusLoading: false,
  changeProjectStatusError: "",
  adminDashboardStatistics: {},
  fetchAdminDashboardStatisticsLoading: false,
  fetchAdminDashboardStatisticsError: "",
  isCollaborationRequestPosted: false,
  postCollaborationRequestLoading: false,
  postCollaborationRequestError: "",
  isReviewPosted: false,
  postReviewLoading: false,
  postReviewError: "",
  isReplyPosted: false,
  postReplyLoading: false,
  postReplyError: "",
  adminDashboardMonthlyStatistics: {},
  fetchAdminDashboardMonthlyStatisticsLoading: false,
  fetchAdminDashboardMonthlyStatisticsError: "",
  projectDistributionCount: {},
  fetchprojectDistributionCountLoading: false,
  fetchprojectDistributionCountError: "",
  searchTerm:null,
  collaborators: [],
  fetchCollaboratorsLoading: false,
  fetchCollaboratorsError: "",
  isRatingPosted: false,
  postRatingLoading: false,
  postRatingError: "",
  adminMonthlyProjectStatistics: {},
  fetchAdminMonthlyProjectsLoading: false,
  fetchAdminMonthlyProjectsError: "",
  studentMonthlyClonesStatistics: {},
  fetchStudentMonthlyClonesLoading: false,
  fetchStudentMonthlyClonesError: "",
  isFeedbackPosted: false,
  postFeedbackLoading: false,
  postFeedbackError: "",
  isAdminUpdated: false,
  updateAdminLoading: false,
  updateAdminError: "",
  isAdminDeleted: false,
  deleteAdminLoading: false,
  deletedAdminError: "",
  isCategoryUpdated: false,
  updateCategoryLoading: false,
  updateCategoryError: "",
  isCallUpdated: false,
  updateCallLoading: false,
  updateCallError: "",
  isFeatureUpdated: false,
  updateFeatureLoading: false,
  updateFeatureError: "",
  isTeamMemberUpdated: false,
  updateTeamMemberLoading: false,
  updateTeamMemberError: "",
  apiKey:"",
  fetchApiKeyLoading:false,
  fetchApiKeyError:"",
};
