import axios from "axios";
const API_URL = "http://173.212.252.138/api";
const token = localStorage.getItem("token");

let settings = {
  baseURL: API_URL,
  headers: {
    Authorization: `Bearer ${token}`
  }
};

export default axios.create(settings);
