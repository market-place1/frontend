import axiosInstance from "./axios_setup";
import axios from "axios";

export const actions = {
  stepper_toggler({ commit }, data) {
    commit("TOGGLE_STEPPER", data);
  },
  stepper_snackbar({ commit }, data) {
    commit("TOGGLE_SNACKBAR", data);
  },
  set_snackbar_message({ commit }, data) {
    commit("SET_SNACKBAR_MESSAGE", data);
  },
  async login({ commit }, data) {
    commit("LOGIN_LOADING", true);
    await axiosInstance
      .post("/login", data)
      .then((res) => {
        console.log(res);
        const token = res.data.success.access_token;
        console.log("Token", token);
        localStorage.setItem("access_token", res.data.success.access_token);
        localStorage.setItem("refresh_token", res.data.success.refresh_token);
        axiosInstance.defaults.headers.common["Authorization"] =
          res.data.success.access_token;
        const user = res.data.success.user;
        console.log("User", user);
        axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
        commit("LOGIN_USER", {
          user,
          token,
        });
        commit("LOGIN_LOADING", false);
      })
      .catch((err) => {
        commit("LOGIN_ERROR", err.response);
        commit("LOGIN_LOADING", false);
      });
  },

  async signUp({ commit }, data) {
    commit("SIGNUP_LOADING", true);
    await axiosInstance
      .post("/register", data)
      .then((res) => {
        console.log(res);
        commit("SIGNUP_STATUS", true);
        commit("SIGNUP_LOADING", false);
      })
      .catch((err) => {
        commit("SIGNUP_LOADING", false);
        commit("SIGNUP_STATUS", false);
        commit("SIGNUP_ERROR", err.response);
      });
  },
  logout({ commit }) {
    localStorage.removeItem("access_token");
    localStorage.removeItem("refresh_token");
    delete axios.defaults.headers.common["Authorization"];
    commit("LOGOUT");
  },
  async postProject({ commit }, data) {
    console.log("Project ", data);
    commit("POST_PROJECT_LOADING", true);
    await axiosInstance
      .post("/create/project", data)
      .then((res) => {
        console.log(res);
        commit("POST_PROJECT_STATUS", true);
        commit("POST_PROJECT_LOADING", false);
      })
      .catch((err) => {
        console.log(err);
        commit("POST_PROJECT_LOADING", false);
        commit("POST_PROJECT_STATUS", false);
        commit("POST_PROJECT_ERROR", err.response);
      });
  },
  async fetchProjects({ commit }) {
    commit("FETCH_PROJECTS_LOADING", true);
    await axiosInstance
      .get("/home/projects")
      .then((res) => {
        console.log(res);
        commit("FETCH_PROJECTS", res.data.projects);
        commit("FETCH_PROJECTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_PROJECTS_LOADING", false);
        commit("FETCH_PROJECTS_ERROR", err.response);
      });
  },
  async fetchProjectDetails({ commit }, id) {
    commit("FETCH_PROJECT_DETAILS_LOADING", true);
    await axiosInstance
      .get("/details/" + id)
      .then((res) => {
        commit("FETCH_PROJECT_DETAILS", res.data);
        commit("FETCH_PROJECT_DETAILS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_PROJECT_DETAILS_LOADING", false);
        commit("FETCH_PROJECT_DETAILS_ERROR", err.response);
      });
  },
  async fetchProjectDetailsWithoutLoading({ commit }, id) {
    await axiosInstance
      .get("/details/" + id)
      .then((res) => {
        commit("FETCH_PROJECT_DETAILS", res.data);
      })
      .catch((err) => {
        commit("FETCH_PROJECT_DETAILS_ERROR", err.response);
      });
  },
  async fetchProjectDocumentation({ commit }, id) {
    commit("FETCH_PROJECT_DOCUMENTATION_LOADING", true);
    await axiosInstance
      .get("/clone/documentation/" + id)
      .then((res) => {
        commit("FETCH_PROJECT_DOCUMENTATION", res.data);
        commit("FETCH_PROJECT_DOCUMENTATION_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_PROJECT_DOCUMENTATION_LOADING", false);
        commit("FETCH_PROJECT_DOCUMENTATION_ERROR", err.response);
      });
  },
  async postTeamMember({ commit }, data) {
    commit("POST_MEMBER_LOADING", true);
    await axiosInstance
      .post("/create/team/member", data)
      .then((res) => {
        console.log(res);
        commit("POST_MEMBER_STATUS", true);
        commit("POST_MEMBER_LOADING", false);
      })
      .catch((err) => {
        commit("POST_MEMBER_LOADING", false);
        commit("POST_MEMBER_STATUS", false);
        commit("POST_MEMBER_ERROR", err.response);
      });
  },
  async fetchTeamMembers({ commit }, id) {
    commit("FETCH_TEAM_MEMBERS_LOADING", true);
    await axiosInstance
      .get("/team/members/" + id)
      .then((res) => {
        commit("FETCH_TEAM_MEMBERS", res.data.members);
        commit("FETCH_TEAM_MEMBERS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_TEAM_MEMBERS_LOADING", false);
        commit("FETCH_TEAM_MEMBERS_ERROR", err.response);
      });
  },
  async fetchClones({ commit }, id) {
    commit("FETCH_CLONES_LOADING", true);
    await axiosInstance
      .get("/deployed/devices/" + id)
      .then((res) => {
        commit("FETCH_CLONES", res.data.deployed_devices);
        commit("FETCH_CLONES_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_CLONES_LOADING", false);
        commit("FETCH_CLONES_ERROR", err.response);
      });
  },
  async fetchFeatures({ commit }, id) {
    commit("FETCH_FEATURES_LOADING", true);
    await axiosInstance
      .get("/all/project/features/" + id)
      .then((res) => {
        commit("FETCH_FEATURES", res.data.features);
        commit("FETCH_FEATURES_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_FEATURES_LOADING", false);
        commit("FETCH_FEATURES_ERROR", err.response);
      });
  },
  async postFeature({ commit }, data) {
    commit("POST_FEATURE_LOADING", true);
    await axiosInstance
      .post("/save/project/features", data)
      .then((res) => {
        console.log(res);
        commit("POST_FEATURE_STATUS", true);
        commit("POST_FEATURE_LOADING", false);
      })
      .catch((err) => {
        commit("POST_FEATURE_LOADING", false);
        commit("POST_FEATURE_STATUS", false);
        commit("POST_FEATURE_ERROR", err);
      });
  },
  async fetchCollaborationCalls({ commit }, id) {
    commit("FETCH_COLLABORATION_CALLS_LOADING", true);
    await axiosInstance
      .get("/all/collaboration/calls/" + id)
      .then((res) => {
        commit("FETCH_COLLABORATION_CALLS", res.data.collaboration_calls);
        commit("FETCH_COLLABORATION_CALLS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_COLLABORATION_CALLS_LOADING", false);
        commit("FETCH_COLLABORATION_CALLS_ERROR", err.response);
      });
  },
  async postCollaborationCall({ commit }, data) {
    commit("POST_COLLABORATION_CALL_LOADING", true);
    await axiosInstance
      .post("/save/collaboration/call", data)
      .then((res) => {
        console.log(res);
        commit("POST_COLLABORATION_CALL_STATUS", true);
        commit("POST_COLLABORATION_CALL_LOADING", false);
      })
      .catch((err) => {
        commit("POST_COLLABORATION_CALL_LOADING", false);
        commit("POST_COLLABORATION_CALL_STATUS", false);
        commit("POST_COLLABORATION_CALL_ERROR", err.response);
      });
  },
  async changeCollaborationCallStatus({ commit }, data) {
    commit("CHANGE_COLLABORATION_CALL_STATUS_LOADING", true);
    await axiosInstance
      .post("/change_collaboration_call_status", data)
      .then((res) => {
        console.log(res);
        commit("CHANGE_COLLABORATION_CALL_STATUS_STATUS", true);
        commit("CHANGE_COLLABORATION_CALL_STATUS_LOADING", false);
      })
      .catch((err) => {
        commit("CHANGE_COLLABORATION_CALL_STATUS_LOADING", false);
        commit("CHANGE_COLLABORATION_CALL_STATUS_STATUS", false);
        commit("CHANGE_COLLABORATION_CALL_STATUS_ERROR", err.response);
      });
  },
  async fetchCollaborationRequests({ commit }, id) {
    commit("FETCH_COLLABORATION_REQUESTS_LOADING", true);
    await axiosInstance
      .get("/all/collaboration/requests/" + id)
      .then((res) => {
        commit("FETCH_COLLABORATION_REQUESTS", res.data.collaboration_requests);
        commit("FETCH_COLLABORATION_REQUESTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_COLLABORATION_REQUESTS_LOADING", false);
        commit("FETCH_COLLABORATION_REQUESTS_ERROR", err.response);
      });
  },
  async changeCollaborationRequestStatus({ commit }, data) {
    commit("CHANGE_COLLABORATION_REQUEST_STATUS_LOADING", true);
    await axiosInstance
      .post("/edit/collaboration/request", data)
      .then((res) => {
        console.log(res);
        commit("CHANGE_COLLABORATION_REQUEST_STATUS_STATUS", true);
        commit("CHANGE_COLLABORATION_REQUEST_STATUS_LOADING", false);
      })
      .catch((err) => {
        commit("CHANGE_COLLABORATION_REQUEST_STATUS_LOADING", false);
        commit("CHANGE_COLLABORATION_REQUEST_STATUS_STATUS", false);
        commit("CHANGE_COLLABORATION_REQUEST_STATUS_ERROR", err.response);
      });
  },
  async fetchReviews({ commit }, id) {
    commit("FETCH_REVIEWS_LOADING", true);
    await axiosInstance
      .get("/comments/" + id)
      .then((res) => {
        commit("FETCH_REVIEWS", res.data.comments);
        commit("FETCH_REVIEWS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_REVIEWS_LOADING", false);
        commit("FETCH_REVIEWS_ERROR", err.response);
      });
  },
  async fetchStudentDashboardStatistics({ commit }, id) {
    commit("FETCH_STUDENT_DASHBORD_STATISTICS_LOADING", true);
    await axiosInstance
      .get("/student/counts/" + id)
      .then((res) => {
        commit("FETCH_STUDENT_DASHBORD_STATISTICS", res.data);
        commit("FETCH_STUDENT_DASHBORD_STATISTICS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_STUDENT_DASHBORD_STATISTICS_LOADING", false);
        commit("FETCH_STUDENT_DASHBORD_STATISTICS_ERROR", err.response);
      });
  },

  async fetchStudents({ commit }) {
    commit("FETCH_STUDENTS_LOADING", true);
    await axiosInstance
      .get("/all/students")
      .then((res) => {
        commit("FETCH_STUDENTS", res.data.students);
        commit("FETCH_STUDENTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_STUDENTS_LOADING", false);
        commit("FETCH_STUDENTS_ERROR", err.response);
      });
  },
  async fetchAdministrators({ commit },) {
    commit("FETCH_ADMINISTRATORS_LOADING", true);
    await axiosInstance
      .get("/all/admin/users")
      .then((res) => {
        commit("FETCH_ADMINISTRATORS", res.data.admins);
        commit("FETCH_ADMINISTRATORS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ADMINISTRATORS_LOADING", false);
        commit("FETCH_ADMINISTRATORS_ERROR", err.response);
      });
  },
  async postAdministrator({ commit }, data) {
    commit("POST_ADMINISTRATOR_LOADING", true);
    await axiosInstance
      .post("/create/admin", data)
      .then((res) => {
        console.log(res);
        commit("POST_ADMINISTRATOR_STATUS", true);
        commit("POST_ADMINISTRATOR_LOADING", false);
      })
      .catch((err) => {
        commit("POST_ADMINISTRATOR_LOADING", false);
        commit("POST_ADMINISTRATOR_STATUS", false);
        commit("POST_ADMINISTRATOR_ERROR", err.response);
      });
  },
  async changeAdministratorStatus({ commit }, data) {
    commit("CHANGE_ADMINISTRATOR_STATUS_LOADING", true);
    await axiosInstance
      .post("/change_administrator_status", data)
      .then((res) => {
        console.log(res);
        commit("CHANGE_ADMINISTRATOR_STATUS_STATUS", true);
        commit("CHANGE_ADMINISTRATOR_STATUS_LOADING", false);
      })
      .catch((err) => {
        commit("CHANGE_ADMINISTRATOR_STATUS_LOADING", false);
        commit("CHANGE_ADMINISTRATOR_STATUS_STATUS", false);
        commit("CHANGE_ADMINISTRATOR_STATUS_ERROR", err.response);
      });
  },
  async fetchAllCollaborators({ commit }) {
    commit("FETCH_ALL_COLLABORATORS_LOADING", true);
    await axiosInstance
      .get("/all/collaborators")
      .then((res) => {
        commit("FETCH_ALL_COLLABORATORS", res.data.collaborators);
        commit("FETCH_ALL_COLLABORATORS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ALL_COLLABORATORS_LOADING", false);
        commit("FETCH_ALL_COLLABORATORS_ERROR", err.response);
      });
  },
  async fetchGuestUsers({ commit }) {
    commit("FETCH_GUEST_USERS_LOADING", true);
    await axiosInstance
      .get("/all/guest/users")
      .then((res) => {
        commit("FETCH_GUEST_USERS", res.data.guests);
        commit("FETCH_GUEST_USERS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_GUEST_USERS_LOADING", false);
        commit("FETCH_GUEST_USERS_ERROR", err.response);
      });
  },
  async fetchCategories({ commit }) {
    commit("FETCH_CATEGORIES_LOADING", true);
    await axiosInstance
      .get("/fetch/categories")
      .then((res) => {
        console.log(res);
        commit("FETCH_CATEGORIES", res.data.categories);
        commit("FETCH_CATEGORIES_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_CATEGORIES_LOADING", false);
        commit("FETCH_CATEGORIES_ERROR", err.response);
      });
  },
  async postCategory({ commit }, data) {
    commit("POST_CATEGORY_LOADING", true);
    await axiosInstance
      .post("/create/category", data)
      .then((res) => {
        console.log(res);
        commit("POST_CATEGORY_STATUS", true);
        commit("POST_CATEGORY_LOADING", false);
      })
      .catch((err) => {
        commit("POST_CATEGORY_LOADING", false);
        commit("POST_CATEGORY_STATUS", false);
        commit("POST_CATEGORY_ERROR", err.response);
      });
  },
  async fetchAllProjects({ commit }) {
    commit("FETCH_ALL_PROJECTS_LOADING", true);
    await axiosInstance
      .get("/all/projects")
      .then((res) => {
        console.log(res);
        commit("FETCH_ALL_PROJECTS", res.data.projects);
        commit("FETCH_ALL_PROJECTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ALL_PROJECTS_LOADING", false);
        commit("FETCH_ALL_PROJECTS_ERROR", err.response);
      });
  },
  async changeProjectStatus({ commit }, data) {
    commit("CHANGE_PROJECT_STATUS_LOADING", true);
    await axiosInstance
      .post("/approve/project", data)
      .then((res) => {
        console.log(res);
        commit("CHANGE_PROJECT_STATUS_STATUS", true);
        commit("CHANGE_PROJECT_STATUS_LOADING", false);
      })
      .catch((err) => {
        commit("CHANGE_PROJECT_STATUS_LOADING", false);
        commit("CHANGE_PROJECT_STATUS_STATUS", false);
        commit("CHANGE_PROJECT_STATUS_ERROR", err.response);
      });
  },
  async fetchAdminDashboardStatistics({ commit }) {
    commit("FETCH_ADMIN_DASHBORD_STATISTICS_LOADING", true);
    await axiosInstance
      .get("/admin/counts")
      .then((res) => {
        commit("FETCH_ADMIN_DASHBORD_STATISTICS", res.data);
        commit("FETCH_ADMIN_DASHBORD_STATISTICS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ADMIN_DASHBORD_STATISTICS_LOADING", false);
        commit("FETCH_ADMIN_DASHBORD_STATISTICS_ERROR", err.response);
      });
  },
  async fetchUserProjects({ commit }, id) {
    commit("FETCH_USER_PROJECTS_LOADING", true);
    await axiosInstance
      .get("/student/projects/" + id)
      .then((res) => {
        commit("FETCH_USER_PROJECTS", res.data.projects);
        commit("FETCH_USER_PROJECTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_USER_PROJECTS_LOADING", false);
        commit("FETCH_USER_PROJECTS_ERROR", err.response);
      });
  },
  selectProject({ commit }, id) {
    commit("SELECT_PROJECT", id);
  },
  async postCollaborationRequest({ commit }, data) {
    commit("POST_COLLABORATION_REQUEST_LOADING", true);
    await axiosInstance
      .post("/make/collaboration/request", data)
      .then((res) => {
        console.log(res);
        commit("POST_COLLABORATION_REQUEST_STATUS", true);
        commit("POST_COLLABORATION_REQUEST_LOADING", false);
      })
      .catch((err) => {
        commit("POST_COLLABORATION_REQUEST_LOADING", false);
        commit("POST_COLLABORATION_REQUEST_STATUS", false);
        commit("POST_COLLABORATION_REQUEST_ERROR", err.response);
      });
  },

  async postReview({ commit }, data) {
    commit("POST_REVIEW_LOADING", true);
    await axiosInstance
      .post("/create/comment", data)
      .then((res) => {
        console.log(res);
        commit("POST_REVIEW_STATUS", true);
        commit("POST_REVIEW_LOADING", false);
      })
      .catch((err) => {
        commit("POST_REVIEW_LOADING", false);
        commit("POST_REVIEW_STATUS", false);
        commit("POST_REVIEW_ERROR", err.response);
      });
  },
  async postReply({ commit }, data) {
    commit("POST_REPLY_LOADING", true);
    await axiosInstance
      .post("/create/comment", data)
      .then((res) => {
        console.log(res);
        commit("POST_REPLY_STATUS", true);
        commit("POST_REPLY_LOADING", false);
      })
      .catch((err) => {
        commit("POST_REPLY_LOADING", false);
        commit("POST_REPLY_STATUS", false);
        commit("POST_REPLY_ERROR", err.response);
      });
  },
  async fetchAdminDashboardMonthlyStatistics({ commit }) {
    commit("FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_LOADING", true);
    await axiosInstance
      .get("/admin/monthly/counts")
      .then((res) => {
        commit("FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS", res.data);
        commit("FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_LOADING", false);
        commit("FETCH_ADMIN_DASHBORD_MONTHLY_STATISTICS_ERROR", err.response);
      });
  },
  async fetchProjectDistributionCount({ commit }) {
    commit("FETCH_PROJECT_DISTRIBUTION_COUNT_LOADING", true);
    await axiosInstance
      .get("/admin/project/distribution/counts")
      .then((res) => {
        commit("FETCH_PROJECT_DISTRIBUTION_COUNT", res.data);
        commit("FETCH_PROJECT_DISTRIBUTION_COUNT_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_PROJECT_DISTRIBUTION_COUNT_LOADING", false);
        commit("FETCH_PROJECT_DISTRIBUTION_COUNT_ERROR", err.response);
      });
  },
  searchTerm({ commit }, term) {
    commit("SEARCHTERM", term);
  },
  async fetchCollaborators({ commit }, id) {
    commit("FETCH_COLLABORATORS_LOADING", true);
    await axiosInstance
      .get("/all/project/collaborators/" + id)
      .then((res) => {
        commit("FETCH_COLLABORATORS", res.data.collaborators);
        commit("FETCH_COLLABORATORS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_COLLABORATORS_LOADING", false);
        commit("FETCH_COLLABORATORS_ERROR", err.response);
      });
  },
  async postRating({ commit }, data) {
    commit("POST_RATING_LOADING", true);
    await axiosInstance
      .post("/create/rating", data)
      .then((res) => {
        console.log(res);
        commit("POST_RATING_STATUS", true);
        commit("POST_RATING_LOADING", false);
      })
      .catch((err) => {
        commit("POST_RATING_LOADING", false);
        commit("POST_RATING_STATUS", false);
        commit("POST_RATING_ERROR", err.response);
      });
  },
  
  async fetchAdminMonthlyProjects({ commit }) {
    commit("FETCH_ADMIN_MONTHLY_PROJECTS_LOADING", true);
    await axiosInstance
      .get("/admin/project/distribution")
      .then((res) => {
        console.log(res);
        commit("FETCH_ADMIN_MONTHLY_PROJECTS", res.data.projects);
        commit("FETCH_ADMIN_MONTHLY_PROJECTS_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_ADMIN_MONTHLY_PROJECTS_LOADING", false);
        commit("FETCH_ADMIN_MONTHLY_PROJECTS_ERROR", err.response);
      });
  },
  async fetchStudentMonthlyClones({ commit },id) {
    commit("FETCH_STUDENT_MONTHLY_CLONES_LOADING", true);
    await axiosInstance
      .get("/clone/distribution/"+id)
      .then((res) => {
        console.log(res);
        commit("FETCH_STUDENT_MONTHLY_CLONES", res.data.clones);
        commit("FETCH_STUDENT_MONTHLY_CLONES_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_STUDENT_MONTHLY_CLONES_LOADING", false);
        commit("FETCH_STUDENT_MONTHLY_CLONES_ERROR", err.response);
      });
  },
  async postFeedback({ commit }, data) {
    commit("POST_FEEDBACK_LOADING", true);
    await axiosInstance
      .post("/save/clone/feedback", data)
      .then((res) => {
        console.log(res);
        commit("POST_FEEDBACK_STATUS", true);
        commit("POST_FEEDBACK_LOADING", false);
      })
      .catch((err) => {
        commit("POST_FEEDBACK_LOADING", false);
        commit("POST_FEEDBACK_STATUS", false);
        commit("POST_FEEDBACK_ERROR", err.response);
      });
  },
  async updateAdmin({ commit }, data) {
    commit("UPDATE_ADMIN_LOADING", true);
    await axiosInstance
      .post("/update/admin", data)
      .then((res) => {
        console.log(res);
        commit("UPDATE_ADMIN_STATUS", true);
        commit("UPDATE_ADMIN_LOADING", false);
      })
      .catch((err) => {
        commit("UPDATE_ADMIN_LOADING", false);
        commit("UPDATE_ADMIN_STATUS", false);
        commit("UPDATE_ADMIN_ERROR", err.response);
      });
  },
  async deleteAdmin({ commit }, id) {
    commit("DELETE_ADMIN_LOADING", true);
    await axiosInstance
      .get("/delete/admin/"+id)
      .then((res) => {
        console.log(res);
        commit("DELETE_ADMIN_STATUS", true);
        commit("DELETE_ADMIN_LOADING", false);
      })
      .catch((err) => {
        commit("DELETE_ADMIN_LOADING", false);
        commit("DELETE_ADMIN_STATUS", false);
        commit("DELETE_ADMIN_ERROR", err.response);
      });
  },
  async updateCategory({ commit }, data) {
    commit("UPDATE_CATEGORY_LOADING", true);
    await axiosInstance
      .post("/update/category", data)
      .then((res) => {
        console.log(res);
        commit("UPDATE_CATEGORY_STATUS", true);
        commit("UPDATE_CATEGORY_LOADING", false);
      })
      .catch((err) => {
        commit("UPDATE_CATEGORY_LOADING", false);
        commit("UPDATE_CATEGORY_STATUS", false);
        commit("UPDATE_CATEGORY_ERROR", err.response);
      });
  },
  async updateCall({ commit }, data) {
    commit("UPDATE_CALL_LOADING", true);
    await axiosInstance
      .post("/edit/collaboration/call", data)
      .then((res) => {
        console.log(res);
        commit("UPDATE_CALL_STATUS", true);
        commit("UPDATE_CALL_LOADING", false);
      })
      .catch((err) => {
        commit("UPDATE_CALL_LOADING", false);
        commit("UPDATE_CALL_STATUS", false);
        commit("UPDATE_CALL_ERROR", err.response);
      });
  },
  async updateFeature({ commit }, data) {
    commit("UPDATE_FEATURE_LOADING", true);
    await axiosInstance
      .post("/edit/project/features", data)
      .then((res) => {
        console.log(res);
        commit("UPDATE_FEATURE_STATUS", true);
        commit("UPDATE_FEATURE_LOADING", false);
      })
      .catch((err) => {
        commit("UPDATE_FEATURE_LOADING", false);
        commit("UPDATE_FEATURE_STATUS", false);
        commit("UPDATE_FEATURE_ERROR", err.response);
      });
  },
  async updateTeamMember({ commit }, data) {
    commit("UPDATE_TEAM_MEMBER_LOADING", true);
    await axiosInstance
      .post("/edit/team/member", data)
      .then((res) => {
        console.log(res);
        commit("UPDATE_TEAM_MEMBER_STATUS", true);
        commit("UPDATE_TEAM_MEMBER_LOADING", false);
      })
      .catch((err) => {
        commit("UPDATE_TEAM_MEMBER_LOADING", false);
        commit("UPDATE_TEAM_MEMBER_STATUS", false);
        commit("UPDATE_TEAM_MEMBER_ERROR", err.response);
      });
  },
  async fetchApiKey({ commit },data) {
    commit("FETCH_API_KEY_LOADING", true);
   
    await axiosInstance
      .post("/generate/access_key",data)
      .then((res) => {
        console.log(res);
        commit("FETCH_API_KEY", res.data.accessKey);
        commit("FETCH_API_KEY_LOADING", false);
      })
      .catch((err) => {
        commit("FETCH_API_KEY_LOADING", false);
        commit("FETCH_API_KEY_ERROR", err.response);
      });
  },
};
